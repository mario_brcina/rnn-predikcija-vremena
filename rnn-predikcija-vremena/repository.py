# -*- coding: utf-8 -*-
"""
@author: Mario
"""

import pandas as pd
import os

data_dir = "data/"

# Gradovi u datasetu
cities = ['Aalborg', 'Aarhus', 'Esbjerg', 'Odense', 'Roskilde']

def path_original_data_pickle():
    return os.path.join(data_dir, "weather-denmark.pkl")


def path_original_data_csv():
    return os.path.join(data_dir, "weather-denmark.csv")


def path_resampled_data_pickle():
    return os.path.join(data_dir, "weather-denmark-resampled.pkl")


def _convert_raw_data(path):
    na_values = ['999', '999.0', '999.9', '9999.9']
    
    df_raw = pd.read_csv(path, sep=',', header=0,
                         index_col=False, na_values=na_values)
    
    df = pd.DataFrame()
    
    df['City'] = df_raw['City']
    
    datestr = df_raw['DateTime']
    df['DateTime'] = pd.to_datetime(datestr)
    
    df['Temp'] = df_raw['Temp']
    df['Pressure'] = df_raw['Pressure']
    df['WindSpeed'] = df_raw['WindSpeed']
    df['WindDir'] = df_raw['WindDir']
    
    df.set_index(['City', 'DateTime'], inplace=True)
    
    df.to_pickle(path_original_data_pickle())
    
    return df


def _resample(df):
    df_res = df
    
    df_res = df_res.resample('1T')

    # Popuni nedostajuće vrijednosti interpolacijom
    df_res = df_res.interpolate(method='time')
    
    df_res = df_res.resample('60T')
    
    df_res = df_res.interpolate()
    
    df_res = df_res.dropna(how='all')

    return df_res


def load_original_data(path):
    df_converted = _convert_raw_data(path)
    return df_converted


def load_resampled_data():
    path = path_resampled_data_pickle()

    # Ako postoji serijalizirani (pickle) cach file učitaj taj file 
    if os.path.exists(path):
        df = pd.read_pickle(path)
    else:
        path_to_csv = path_original_data_csv()
        
        df_org = load_original_data(path_to_csv)

        # Djelim originalni dataset u zasebne data frame-ove za svaki grad
        df_cities = [df_org.xs(city) for city in cities]
        
        df_resampled = [_resample(df_city) for df_city in df_cities]

        # Spajam nazad podijeljene data frameove za svaki grad nakon resamplinga 
        # u jedinstveni data frame koji će se koristiti za analizu
        df = pd.concat(df_resampled, keys=cities,
                       axis=1, join='inner')

        df.to_pickle(path)

    return df




